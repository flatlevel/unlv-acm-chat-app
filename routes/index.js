var express = require('express');
var properties = require('../config/config.js');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res, next) {
  res.redirect('public');
});

module.exports = router;
