/**
 * All of the behavior of our app is controlled here. I am using JQuery to
 * handle selecting information from the DOM tree. JQuery is extremely useful,
 * but remember you can interact with the DOM using native javascript with the
 * `Document` global object.
 *
 * It is important to enclose your code on clientside javascript. The following
 * pattern is considered the `vanilla` design pattern for modularizing javascript.
 * The returned function is treated as a constructor, and the inner code is
 * considered properties and object methods, but keep in mind that javascript
 * is not like most Object-Oriented languages.
 *
 */
var ChatApp = (function() {
  'use strict'; // Avoid dangerous javascript, such as manually deleting
                // heap objects. (This is a garbage-collected environment)

  // App property declarations.
  var submitBtn,
      nameField,
      msgField,
      chat,
      socket,
      NEW_CHAT_FIELD = '<li>'
        + '<p class="chatLog"><strong></strong> <small></small><br>'
        + '<span></span></p>'
        + '</li>';

  function addChatField(data) {
    // Add a new chat field HTML to the DOM, and fill in the content
    // There are many ways to do this, I chose this way because it
    // seems to be the most straight forward.
    chat.append($(NEW_CHAT_FIELD));
    var latestChat = chat.children().last();

    latestChat.find('strong').text(data.username);
    latestChat.find('small').text(data.date);
    latestChat.find('span').text(data.message);

  }

  function handleBadResponse(info) {
    console.log("[ERROR]", info);
    alert('Error submitting chat.\n');
  }

  function submitChat(event) {
    var name =  nameField.val();
    var msg =   msgField.val();

    // Prevent default form submit action from happening.
    // (We don't want the page refreshed.)
    event.preventDefault();

    // Double check one more time that our username is valid.
    // Also, make sure the user actually entered something for us
    // to send to the server!
    if (isValidName() && msg.length > 0) {
      console.log("[CHAT]", name, msg);

      var emitData = {
        username:   name,
        message:    msg
      };

      // This will send the chat data to the server for processing.
      socket.emit('message', emitData);
    }
  }

  function isValidName() {
    // Regular expressions make string processing very easy. This is an
    // example of using one to check that our username is valid.
    //
    // All this regular expression requires is:
    //  - An alphanumeric character
    //  - Underscore `_` or en-dash `-`
    //  - At least 4 characters, and no bigger than 16 characters.
    var name = nameField.val();
    var valid = /^[a-zA-Z0-9_-]{4,16}$/.test(name);

    submitBtn.prop('disabled', !valid);
    return valid;
  }

  return function() {
    // get javascript bindings of HTML.
    submitBtn =   $("#chatForm button[type='submit']");
    nameField =   $("#inputName");
    msgField =    $("#inputChat");
    chat =        $("#chatWindow ul");

    // User needs to enter their name first!
    submitBtn.prop('disabled', true);

    // Bind our `submitChat` function to this button's onClick event.
    submitBtn.click(submitChat);

    // We want to enable the button when the user has typed a valid
    // name into the field.
    nameField.keyup(isValidName);

    // Init the sockect connection
    socket = io();
    socket.on('message', addChatField);
    socket.on('error', handleBadResponse);
  };
})();

// Init!
new ChatApp();
