# Realtime Chat App

For UNLV ACM

### How to run:
First, clone this repo and go into the root directory you cloned it at. Then run the following:

     `npm install`

To install the dependencies. To start the app, run:

     `node app.js`

### Questions?

You can email me here: [geoff@skyworksas.com](mailto:geoff@skyworksas.com)