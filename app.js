/**
 * This serves as the entry point of our server code. This is where
 * your express app is initialized and the webapp is bootstrapped.
 *
 * @author Geoff Gardner
 */
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var apiErrorHandler = require('api-error-handler');
var config = require('./config/config.js');
var routes = require('./routes/index');

var env = process.env.NODE_ENV || 'development';

var app = express();
// Require some modules for socket io.
var http = require('http').Server(app);
var io = require('socket.io')(http);

// set our environment (this is important for deployment)
app.set('env', env);
app.set('port', config.port || 3000);

// Initialize the middlewares.
// middlewares are invoked before any routes are, so think of this as all of
// the `preprocessing` that the server does before the request is completed.
app.use(logger('dev')); // Gives us a nice feed of the incoming and outcoming traffic.
app.use(bodyParser.json()); // Allows us to decode JSON POST requests.
app.use(bodyParser.urlencoded({ extended: false })); // Decode urlencoded POST requests (query strings).
app.use(express.static(path.join(__dirname, 'public'))); // Render HTML/CSS/JS to the client.
app.use(apiErrorHandler()); // used to send proper responses back to the client.

// Start the router!
app.use('/', routes);


// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers
// NOTE currently these handlers are configured only for 404s. Usually,
// template engines are employed for 500s (server-side errors), however I didn't
// want to cover them here. Maybe that is something you can add?

// Development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log(err);
    res
      .status(err.status || 500)
      .sendFile(path.join(__dirname, '/public', '404.html'));

  });
}

// Production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res
    .status(err.status || 500)
    .sendFile(path.join(__dirname, '/public', '404.html'));
});

// Socket IO is entirely event driven. First, we used connnection
// event to show a new user has connected. Then, we add a listener
// for a custom event we simply call message, that is emitted by
// the client when they type a new chat message.
io.on('connection', function(socket) {
  console.log('[CHAT] User connected.');

  socket.on('message', function(message) {
    // Add a timestamp to the message
    message.date = new Date();

    // Catch message, and send it back to all peers
    io.emit('message', message);
  });
});

// Init the server!
var server = http.listen(app.get('port'), function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Chat App listening at http://%s:%s', host, port);

});
