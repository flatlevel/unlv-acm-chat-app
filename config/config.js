/**
 * This file is typically used for middleware initialization and configuration.
 * Since we have none, it is reserved mainly for fetching the properties.json,
 * which contain's our webapp's global configuration settings.
 */
var fs = require('fs'),
    config = './properties.json',
    properties = require(config);

var exports = module.exports = properties;
